const axios = require('axios');

async function cloneIssues() {
    try {        // Make GET request to fetch Issues details
        const response = await axios.get('https://gitlab.com/api/v4/projects/' + process.env.CI_PROJECT_ID + '/issues', {
            headers: {
                'PRIVATE-TOKEN': process.env.GLPAT_ISSUES
            }
        });

        // Iterate over each Issue
        for (const issue of response.data) { 
        // Check if the label 'issue-template' exists
            if (issue.labels.includes('issue-template') && issue.state == "opened") {
            console.log("Label found for issue with ID:", issue.iid);
            
            // Clone issue
            const response = await axios.post('https://gitlab.com/api/v4/projects/' + process.env.CI_PROJECT_ID + '/issues/' + issue.iid + '/clone?to_project_id=' + process.env.CI_PROJECT_ID, null, {
                headers: {
                    'PRIVATE-TOKEN': process.env.GLPAT_ISSUES
                }
            });
            console.log('Issue cloned!');
            }
        }  
    } catch (error) {
        console.error('Error:', error.message);
    }
}

// Call the function to clone issues
cloneIssues();
